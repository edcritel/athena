/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "../RootCollection.h"
#include "../RNTCollection.h"

using pool::RootCollection::RootCollection;
DECLARE_COMPONENT_WITH_ID(RootCollection, "RootCollection")

using pool::RootCollection::RNTCollection;
DECLARE_COMPONENT_WITH_ID(RNTCollection, "RNTCollection")

/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_sTgcMeasurementFWD_H
#define XAODMUONPREPDATA_sTgcMeasurementFWD_H

/** @brief Forward declaration of the xAOD::sTgcMeasurement */
namespace xAOD{
   class sTgcMeasurement_v1;
   using sTgcMeasurement = sTgcMeasurement_v1;

   class sTgcMeasurementAuxContainer_v1;
   using sTgcMeasurementAuxContainer = sTgcMeasurementAuxContainer_v1;
}
#endif

/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ACTSTRK_SPCACHEID_H
#define ACTSTRK_SPCACHEID_H

#include "xAODInDetMeasurement/SpacePoint.h"
#include "src/Cache.h"

CLASS_DEF(ActsTrk::Cache::Handles<xAOD::SpacePoint>::IDCBackend, 202232990, 1);
#endif
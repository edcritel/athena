/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#include "src/detail/AtlasUncalibSourceLinkAccessor.h"

#include <algorithm>

namespace ActsTrk::detail {

inline SharedHitCounter::SharedHitCounter(std::size_t nMeasurementContainerMax)
    : m_measurementIndex(nMeasurementContainerMax) {}

inline MeasurementIndex& SharedHitCounter::measurementIndexer() {
  return m_measurementIndex;
}

inline const MeasurementIndex& SharedHitCounter::measurementIndexer() const {
  return m_measurementIndex;
}

inline void SharedHitCounter::addMeasurements(const xAOD::UncalibratedMeasurementContainer &clusterContainer) {
  m_measurementIndex.addMeasurements(clusterContainer);
}

template <Acts::TrackContainerFrontend track_container_t>
inline std::pair<std::size_t, std::size_t> SharedHitCounter::computeSharedHits(typename track_container_t::TrackProxy& track,
                                                                               track_container_t& tracks) {
  // Based on ActsExamples::TrackFindingAlgorithm::computeSharedHits().
  // Finds shared hits in the reconstructed track and updates SharedHitFlag in both tracks.
  // Uses m_measurementIndex to convert hit to hitIndex, and then m_firstTrackStateOnTheHit[] to convert hitIndex -> TrackStateIndex
  // Returns stats: nShared and nBadTrackMeasurements

  constexpr TrackStateIndex noTrackState{std::numeric_limits<std::size_t>::max(), std::numeric_limits<std::size_t>::max()};

  if (m_firstTrackStateOnTheHit.size() < m_measurementIndex.size())
    m_firstTrackStateOnTheHit.resize(m_measurementIndex.size(), noTrackState);

  std::size_t nShared = 0;
  std::size_t nBadTrackMeasurements = 0;
  for (auto state : track.trackStatesReversed()) {
    if (!state.typeFlags().test(Acts::TrackStateFlag::MeasurementFlag))
      continue;

    if (!state.hasUncalibratedSourceLink())
      continue;

    auto sl = state.getUncalibratedSourceLink().template get<ATLASUncalibSourceLink>();
    const xAOD::UncalibratedMeasurement& hit = getUncalibratedMeasurement(sl);
    std::size_t hitIndex = m_measurementIndex.index(hit);
    if (!(hitIndex < m_measurementIndex.size())) {
      // Save ATH_MSG_ERROR() for the caller, since for simplicity and efficiency this class doesn't have access to AthMsgStream.
      // std::cout << "ERROR hit index " << hitIndex << " past end of " << m_measurementIndex.size() << " hit indices\n";
      ++nBadTrackMeasurements;
      continue;
    }

    // Check if hit not already used
    if (m_firstTrackStateOnTheHit[hitIndex].trackIndex == noTrackState.trackIndex) {
      m_firstTrackStateOnTheHit[hitIndex] = {track.index(), state.index()};
      continue;
    }

    // if already used, control if first track state has been marked as shared
    const TrackStateIndex& indexFirstTrackState = m_firstTrackStateOnTheHit[hitIndex];
    if (!(indexFirstTrackState.trackIndex < tracks.size())) {
      // std::cout << "ERROR track index " << indexFirstTrackState.trackIndex << " past end of " << tracks.size() << " tracks in container\n";
      ++nBadTrackMeasurements;
      continue;
    }

    auto firstTrack = tracks.getTrack(indexFirstTrackState.trackIndex);
    auto& firstStateContainer = firstTrack.container().trackStateContainer();
    if (!(indexFirstTrackState.stateIndex < firstStateContainer.size())) {
      // std::cout << "ERROR track state index " << indexFirstTrackState.stateIndex << " past end of " << firstStateContainer.size() << " track states in container\n";
      ++nBadTrackMeasurements;
      continue;
    }

    auto firstState = firstStateContainer.getTrackState(indexFirstTrackState.stateIndex);
    if (!firstState.typeFlags().test(Acts::TrackStateFlag::SharedHitFlag)) {
      firstState.typeFlags().set(Acts::TrackStateFlag::SharedHitFlag);
      firstTrack.nSharedHits()++;
      ++nShared;
    }

    // Decorate this track state
    state.typeFlags().set(Acts::TrackStateFlag::SharedHitFlag);
    track.nSharedHits()++;
    ++nShared;
  }

  return {nShared, nBadTrackMeasurements};
}

}  // namespace ActsTrk::detail
